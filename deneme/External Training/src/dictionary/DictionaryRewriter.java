package dictionary;

import java.io.IOException;

public class DictionaryRewriter {
    public static void main(String[] args) throws IOException {
        System.out.println("Rewriting dictionary slow...");
        long slowStartTime = System.nanoTime();
        new DictionaryRewriterSlow().rewriteDictionary();
        long slowStopTime = System.nanoTime();
        long slowTimeInSeconds = Math.round(((slowStopTime - slowStartTime) / 1E9));
        System.out.println("done in " + slowTimeInSeconds + "s");

        System.out.println("Rewriting dictionary fast...");
        long fastStartTime = System.nanoTime();
        new DictionaryRewriterFast().rewriteDictionary();
        long fastStopTime = System.nanoTime();
        long fastTimeInSeconds = Math.round(((fastStopTime - fastStartTime) / 1E9));
        System.out.println("done in " + fastTimeInSeconds + "s");

        long scoreInPercent = Math.round((((double) fastTimeInSeconds) / slowTimeInSeconds) * 100);
        System.out.println("Your score: " + scoreInPercent + "%");
    }
}
