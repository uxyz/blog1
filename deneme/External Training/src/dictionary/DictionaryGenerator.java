package dictionary;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class DictionaryGenerator {
    public static final String DICTIONARY_FILENAME = "C:/Users/Umut/Desktop/superdictionary.txt";
    public static final int MAX_WORD_LENGTH = 8;
    public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    public static final long LINE_COUNT = 20_000;

    private Random random;

    public static void main(String[] args) throws IOException {
        new DictionaryGenerator().run();
    }

    public DictionaryGenerator() {
        this.random = new Random();
    }

    private void run() throws IOException {
        long linesWritten = 0;
        File file = new File(DICTIONARY_FILENAME);

        if (file.exists()) {
            System.out.println("Datei " + DICTIONARY_FILENAME + " existiert bereits.");
            System.exit(0);
        }

        System.out.print("Das Wörterbuch wird erzeugt, bitte warten...");

        try (FileWriter fileWriter = new FileWriter(file)) {
            while (linesWritten < LINE_COUNT) {
                String wordPair = generateWordPair();
                fileWriter.write(wordPair);
                linesWritten++;
            }
        }

        System.out.println("fertig");
    }

    private String generateWordPair() {
        return generateWord() + "=" + generateWord() + "\n";
    }

    private String generateWord() {
        int wordLen = random.nextInt(MAX_WORD_LENGTH) + 1;
        String word = "";
        for (int i = 0; i < wordLen; i++) {
            word += generateChar();
        }
        return word;
    }

    private char generateChar() {
        int index = random.nextInt(ALPHABET.length() - 1);
        return ALPHABET.charAt(index);
    }
}
