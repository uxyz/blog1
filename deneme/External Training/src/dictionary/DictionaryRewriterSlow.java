package dictionary;

import java.io.*;

public class DictionaryRewriterSlow {
    private static final String REWRITTEN_DICTIONARY_FILENAME = new String("C:/Users/Umut/Desktop/superdictionary_rewritten.txt");

    public void rewriteDictionary() throws IOException {
        File srcFile = new File(DictionaryGenerator.DICTIONARY_FILENAME);
        File destFile = new File(REWRITTEN_DICTIONARY_FILENAME);

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(srcFile));
             FileWriter fileWriter = new FileWriter(destFile)) {
            int lineNo = 0;
            while (bufferedReader.ready()) {
                String line = bufferedReader.readLine();
                lineNo++;
                String[] words = line.split("=");
                if (!wordExists(srcFile, words[1], lineNo - 1)) {
                    fileWriter.write(words[1] + "=" + words[0] + "\n");
                }
                if (lineNo % 1000 == 0) {
                    System.out.println(lineNo);
                }
            }
        }
    }

    private boolean wordExists(File srcFile, String word, int maxLineNo) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(srcFile))) {
            int lineNo = 0;
            while (bufferedReader.ready()) {
                String line = bufferedReader.readLine();
                lineNo++;
                if (lineNo >= maxLineNo) {
                    break;
                }
                String[] words = line.split("=");
                if (word.equals(words[1])) {
                    return true;
                }
            }
            return false;
        }
    }
}
