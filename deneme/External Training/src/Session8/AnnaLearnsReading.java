package Session8;



public class AnnaLearnsReading {

    static private int WORDS = 5;
    static private int DAYS = 23;

    public static void main(String[] args) {
        calwithwords();
        calcWithDays();

    }


    private static void calwithwords(){
        int day =1;
        for (int i =1; i <WORDS; i++){
            day +=i;
        }
        System.out.println("Anna liest "+ WORDS + " Wörter am Tag " +day);

    }

    private static void calcWithDays(){
        int day = DAYS;
        int count = 1;

        while (day >0){
            day-=count;
            count++;
        }
        System.out.println("Anna liest "+ (count-1) +" Wörter am Tag " +DAYS);
    }
}
