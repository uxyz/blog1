package themen;

public class User {


    private String name;
    private String nachName;
    private int alter;


    public User(String name, String nachName, int alter) {
        this.name = name;
        this.nachName = nachName;
        this.alter = alter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNachName() {
        return nachName;
    }

    public void setNachName(String nachName) {
        this.nachName = nachName;
    }

    public int getAlter() {
        return alter;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }



}
