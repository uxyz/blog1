package themen;



import java.util.*;
import java.util.stream.Collectors;

public class Stream {


private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    private List<User> userList = Arrays.asList();


    public Stream(User user) {
        this.user = user;
    }

    public static void main(String[] args) {


        List isimler = Arrays.asList("Umut", "Kordula", "Katha", "Chilli");

        // isimler.add("Ziya"); das geht nicht

        isimler.stream()
                .forEach(System.out::println);

        isimler.stream().forEach(x ->{
            System.out.println(x);
        });

        System.out.println(isimler);


        List isimler1 = new ArrayList();
        isimler1.add("Umuti");
        isimler1.add(1);
        isimler1.add("1");

        isimler1.stream()
                .forEach(System.out::println);

        System.out.println(isimler1);

        List<String> isimler2 = new ArrayList<>();

        isimler2.add("Umut");


        isimler.stream()
                .limit(2)
                .forEach(System.out::println);

        isimler.stream()
                .sorted().forEach(System.out::println);

        long total = isimler.stream().count();
        System.out.println(total);

        //Map<Integer, List> integerListMap = isimler.stream().collect(Collectors.groupingBy(x->x.length()));


        User user1 = new User("Umut", "Özdemir",35);
        User user2 = new User("Kordula","Berger",39);
        User user3 =new User("Katharina","Becker",38);

        System.out.println(user1);

        List<User> userList = Arrays.asList(user1,user2,user3);

        System.out.println(userList);





        userList.stream().filter(user -> (user.getNachName().contains("B"))).collect(Collectors.toList()).forEach(System.out::println);

        System.out.println(user1.getNachName());

    }
}
