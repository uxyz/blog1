import com.sun.org.apache.xpath.internal.objects.XBoolean;

import java.util.*;

public class Solution {

    public static int solution(int[] A, int X, int Y, int Z) {

        Map<Character, int []> characterMap = new HashMap<>();
        characterMap.put('X',new int[] {X,0});
        characterMap.put('Y',new int[] {Y,0});
        characterMap.put('Z',new int[] {Z,0});

        Queue<Map.Entry<Character, int[]>> min = new PriorityQueue<>((a, b)->a.getValue()[0] == b.getValue()[0] ? a.getValue()[1] - b.getValue()[1] : a.getKey() - b.getKey());
        min.addAll(characterMap.entrySet());
        int[] newone = new int[3];
        List<Map.Entry<Character, int[]>> list = new ArrayList<>();
        for(int i=0;i<A.length;i++) {
            int cur = A[i];
            while(!min.isEmpty()) {
                if(!min.isEmpty() && min.peek().getValue()[0] < cur) {
                    list.add(min.poll());
                    if(min.isEmpty())
                        return -1;
                }
                else {
                    Map.Entry<Character, int[]> e = min.poll();
                    e.getValue()[0] -= cur;
                    e.getValue()[1] += cur;
                    list.add(e);
                    if(i == A.length-1)
                        break;
                    newone[e.getKey() - 'X'] += cur;
                    min.addAll(list);
                    list.clear();
                    break;
                }
            }
        }
        return Math.max(newone[0], Math.max(newone[1], newone[2]));

    }

    public static void main(String[] args) {
        int[] A1 = {2,8,4,3,2};
        int x1=7, y1=11, z1=3;
        System.out.println(solution(A1, x1, y1, z1));

    }
}
