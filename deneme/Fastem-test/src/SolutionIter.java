import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class SolutionIter implements Iterable<Integer> {





    private Reader inp;
    public SolutionIter(Reader inp){
        this.inp = inp;

    };

    public Iterator<Integer> iterator(){
        return new IteratorX();
    }

    public class IteratorX implements Iterator<Integer>{
        private List<Integer> list = new ArrayList<Integer>();
        private Reader value;
        private int position = -1;

        public IteratorX(){
            this.value = SolutionIter.this.inp;
            list = read(value);
            list.get(0);
        }

        public boolean hasNext(){
            try{
                int y = list.get(position+1);
                return true;
            }
            catch(Exception ex){
                return false;
            }
        }

        public Integer next(){
            try{
                position++;
                return list.get(position);
            }
            catch (Exception ex){
                throw new NoSuchElementException();
            }
        }

        private List<Integer> read(Reader value){
            char z;
            List<Integer> integerList = new ArrayList<Integer>();
            String valueString="";
            try{
                int valuex =value.read();
                while(-1!=valuex){
                    char valueChar = (char) valuex;
                    valueString+=valueChar;
                    valuex = value.read();


                }
            }
            catch(IOException e){

            }

            String[] line = valueString.split("\\r?\\n");

            for(String str : line){
                str = str.trim();
                try{
                    int y = Integer.parseInt(str);
                    if (y >=-1000000000 && y<=1000000000){
                        integerList.add(y);
                    }
                }catch(Exception e){}
            }
            return integerList;
        }
    }

    public static void main (String[] args) throws java.lang.Exception
    {
        String inputfile = "137\n-104\n2 58\n  +0\n++3\n+1\n 23.9\n2000000000\n-0\nfive\n -1";
        Reader reader = new StringReader(inputfile);
        for (Integer x : new SolutionIter(reader))
        {
            System.out.println(x);
        }
    }
}



