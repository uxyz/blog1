import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-like-button',
  templateUrl: './like-button.component.html',
  styleUrls: ['./like-button.component.css']
})
export class LikeButtonComponent implements OnInit {

  public initialCount = 100 ;

  likesnumber: number;

  constructor() { }


  addLikes(){
    this.initialCount++;
  }

  removeLikes(){
    this.initialCount--;
  }


  ngOnInit(): void {
  }

}
