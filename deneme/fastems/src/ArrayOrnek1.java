import javax.swing.*;

public class ArrayOrnek1 {
    public static void main(String[] args) {
        int [] dizi = new int [5];

        for(int i = 0;i<dizi.length;i++){  // bu i dedigimiz index, elemanlara ulasacagaimiz nokta


            // bu for döngüleri cok onemli, bir kac tane ard arda, ic ice kullanabilirsin.
            dizi[i] = Integer.parseInt(JOptionPane.showInputDialog(i + ". sayi"));
        }
        for(int i = 0;i<dizi.length;i++){
            System.out.println("Sayi [ " + i + "] = " + dizi[i]);
        }
    }
}
