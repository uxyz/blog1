import java.util.Arrays;

public class Solution1 {

    public static  int[] solution(int[] A, int K){ // geri donus tipi int array olan solution metodunu olusturuyoruz.
        // bu metod iki tane parametre aliyor, A adin da bir integer array, ve K integer sayi

        int [] B = null; // bu metodun gövdesin de, icinde integer sayilar bulunabilen bir B arrayi olusturdum, null dedim

        for( int i=0; i<K;i++){     //sonra for ile verilen K kadar yapacagim isi belirliyorum
            B = rotate(A);          // bu islem diyorki, B arrayi varya onu rotate metoduna A arrayini parametre vererek hesapliyorsun


        }
        return B;       //hesapladiktan sonra bu B integer arrayini metodun donusu olarak ver
    }

    public static int [] rotate(int[] arr){        //rotate metodumuzda geri donus tipi olarak integer array veriyor, ve parametre olarak
        //arr isimli integer arrayini aliyor

        int x = arr[arr.length-1], i; // sonra bu metodun govdesin de iki tane int deger tanimiliyorum
        // x degeri parametre olarak verilen arr in son elemanina esit. ikinci olarak i adin da bir integer deklare ediyorum

        for (i = arr.length-1;i>0;i--){     // for dongusu kuruyorum, baslangic degeri array uzunlugundan bi eksik, ve azalarak gidiyor
                                            // mesela 4 elemanli bir arrayda i 3 ten basliyor
            arr [i] = arr[i-1];             // bu demekki arrayin 3. elemani 2. elemanina, 2. elemani 1. elemana, 1. eleman da , 0. elemana esit
                                            //
            arr[0]=x;
        }
        return arr;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(solution(new int []{3,8,9,7},1)));
    }
}
