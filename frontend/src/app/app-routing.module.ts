import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {UeberschriftSchreibenComponent} from './ueberschrift-schreiben/ueberschrift-schreiben.component';
import {UeberschriftDetailsComponent} from './ueberschrift-details/ueberschrift-details.component';
import {EintragSchreibenComponent} from './eintrag-schreiben/eintrag-schreiben.component';
import {UserLikeComponent} from './user-like/user-like.component';
import {HomeComponent} from './home/home.component';
import {LeftFrameComponent} from './left-frame/left-frame.component';
import {NextpageComponent} from './nextpage/nextpage.component';
import {MessageComponent} from './message/message.component';
import {MessageBoxComponent} from './message-box/message-box.component';
import {InoutboxComponent} from './inoutbox/inoutbox.component';
import {MessageLargeComponent} from './message-large/message-large.component';

const routes: Routes = [

  {path: 'registration', component: RegistrationComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LoginComponent},
  {path: 'register', component: RegistrationComponent},
    {path: 'ueberschrift/:id', component: UeberschriftSchreibenComponent},
  {path: 'ueberschrift/:id/:eintragid', component: UeberschriftSchreibenComponent},
  // {path: 'ueberschrift-schreiben/:id', component: UeberschriftSchreibenComponent},
  {path: 'ueberschriftDetail/:id', component: UeberschriftDetailsComponent},
   {path: 'message/:id', component: MessageBoxComponent},


  // {path: 'eintrag/:id/:uid', component: EintragSchreibenComponent},
   {path: 'eintrag-schreiben/:id/:uid/:eintragid', component: EintragSchreibenComponent},
  {path: 'user-like/:id', component: UserLikeComponent},
  {path: '', component: HomeComponent},
  {path: 'ueberschrifts/:id', component: NextpageComponent},
  {path: 'message/:id/:uid', component: MessageComponent},
  {path: 'messageout/:id', component: InoutboxComponent},
  {path: 'messagelarge/:id', component: MessageLargeComponent}

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],  exports: [RouterModule]

})
export class AppRoutingModule { }
