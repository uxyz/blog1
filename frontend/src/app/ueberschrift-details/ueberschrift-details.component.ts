import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {Ueberschrift} from '../ueberschrift';
import {Eintrag} from '../eintrag';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../security.service';
import {UeberschriftService} from '../ueberschrift.service';
import {EintragService} from '../eintrag.service';
import {UserLike} from '../userLike';

@Component({
  selector: 'app-ueberschrift-details',
  templateUrl: './ueberschrift-details.component.html',
  styleUrls: ['./ueberschrift-details.component.css']
})
export class UeberschriftDetailsComponent implements OnInit {


  sessionUser: User | null = null;
  private id: number;
  private sub: any;
  private idString: string;
  ueberschrift: Ueberschrift;
  eintragList: Eintrag[];
  eintrag: Eintrag;
  likesnumber: number[] = [];
  likeseintrags: number [] = [];
  likesuserid: number[] = [];
  flashMessage = {message: ''};
  eintragidstring: string;
  likeUserE: UserLike[] = [];
  likeUserE1: UserLike[];
  idu: number;
  idus: string;


  constructor(private http: HttpClient, private securityService: SecurityService, private router: Router,
              private route: ActivatedRoute) {
  }

  deleteEitrag(eintrag){
    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    this.idus = eintrag.ueberschrift.id.toString();
    this.http.post<Eintrag[]>('/api/deleteeintragd/' + this.eintragidstring + '/' + userid + '/' + this.idus, null)
      .subscribe(eintr => this.eintragList = eintr );
    // this.http.post<UserLike[]>('/api/deletelikes/' + this.eintragidstring + '/' + userid, null);

    console.log(userid);
    console.log(this.eintragidstring);

  }


  addToLikes(eintrag) {

    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    console.log(this.eintragidstring);
    this.http.post<UserLike[]>('/api/likesnumber/' + this.eintragidstring + '/' + userid, null)
      .subscribe(eintraglike => {
        this.likeUserE = eintraglike;

        for (const likeEintrag of eintraglike) {
          this.likeseintrags.push(likeEintrag.eintrag.id);
        }
      });

  }
  removelike(eintrag){
    this.likeUserE.splice(eintrag);
    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    this.http.post<UserLike[]>('/api/unlikesnumber/' + this.eintragidstring + '/' + userid, null)
      .subscribe(eintraglike => {
        this.likeUserE = eintraglike;
        this.router.navigateByUrl('/');
      });
  }



  ngOnInit(): void {

    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
        if (u != null) {
          const idUser = this.sessionUser.id.toString();
          this.http.get<UserLike[]>('/api/userlikes/' + idUser)
            .subscribe(userlikes => {
              this.likeUserE = userlikes;
              for (const usrlik of userlikes) {
                this.likeseintrags.push(usrlik.eintrag.id);
                if (!this.likesnumber.includes(usrlik.eintrag.id)) {
                  this.likesnumber.push(usrlik.eintrag.id);
                }
                if (!this.likesuserid.includes(this.sessionUser.id)) {
                  this.likesuserid.push(this.sessionUser.id);
                }
              }
            });

        }


      });
    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;
    });
    this.idString = this.id.toString();


    this.http.get<Ueberschrift>('/api/ueberschrift/' + this.idString)
      .subscribe(ueberschrift => {
        this.ueberschrift = ueberschrift;

        console.log(this.ueberschrift);
        console.log(this.ueberschrift.userid);
        console.log(this.ueberschrift.username);




      });


    this.http.get<Eintrag[]>('/api/eintrag/' + this.idString)
      .subscribe(
        eintragList => {
          this.eintragList = eintragList;

          for (const  eint of this.eintragList){
            console.log(eint);
            console.log(eint.user_id);
           // console.log(this.sessionUser.id);
          //  console.log(eint.username);
          //  console.log(this.sessionUser.username);
          }
        });

  }

  setFlashMessage(message: string) {
    this.flashMessage.message = message;
  }
}
