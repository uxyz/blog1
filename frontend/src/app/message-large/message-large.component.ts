import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {Message} from '../message';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-message-large',
  templateUrl: './message-large.component.html',
  styleUrls: ['./message-large.component.css']
})
export class MessageLargeComponent implements OnInit {


  sessionUser: User | null = null;
  private id: number;
  private sub: any;
  private idString: string;
  username: string;
  messages: Message[] = [];
  messageList: Message[];
  message: Message;
  readmessage: boolean;

  constructor(private http: HttpClient, private securityService: SecurityService, private router: Router,
              private route: ActivatedRoute) {
    this.readmessage = false;
  }

  ngOnInit(): void {

    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      });

    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;

      this.idString = this.id.toString();
    });

    this.http.get<Message>('/api/messageread/' + this.idString)

      .subscribe(message => {

        this.message = message;
      });

  }
}
