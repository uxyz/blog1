import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {UeberschriftDTO} from '../ueberschriftDTO';
import {Ueberschrift} from '../ueberschrift';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../security.service';
import {Eintrag} from '../eintrag';

@Component({
  selector: 'app-ueberschrift-schreiben',
  templateUrl: './ueberschrift-schreiben.component.html',
  styleUrls: ['./ueberschrift-schreiben.component.css']
})
export class UeberschriftSchreibenComponent implements OnInit {


  sessionUser: User | null = null;
  sub: any;
  private id: number;
  private idString: string;
  private eintragid: number;
  private idEString: string;
  private username: string;
  eintragList: Eintrag[];
  private ide: number;
  private ides: string;


  ueberschriftData: UeberschriftDTO = {
    text: '',
  };

  postedAt: Date = new Date();

  flashMessage = {message: ''};

  private ueberschrift: Ueberschrift;

  setFlashMessage(message: string) {
    this.flashMessage.message = message;
  }

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute, private securityService: SecurityService) {
  }

  ngOnInit(): void {

    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      }
    );

    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;
    });
    this.idString = this.id.toString();

    this.sub = this.route.params.subscribe(params => {
      this.eintragid = +params.eintragid;
    });
    this.idEString = this.eintragid.toString();
    console.log(this.eintragid);

    this.http.get<Eintrag[]>('/api/eintrag/')
      .subscribe(
        eintragList => {
          this.eintragList = eintragList;
          this.ide = this.eintragList.length + 1;

          this.ides = this.ide.toString();
        });


  }


  ueberschriftschreiben() {
    const idUser = this.sessionUser.id.toString();
    if (this.ueberschriftData.text.length < 1) {
      this.setFlashMessage('you must enter something');
    } else {
      this.http.post<Ueberschrift>('/api/ueberschrift/' + idUser, {
        text: this.ueberschriftData.text,
        userid: idUser,
        postedAt: this.postedAt,
        username: this.sessionUser.username
      })
        .subscribe(
          ueberschrift => {
            this.ueberschrift = ueberschrift;
            if (ueberschrift != null) {
              this.setFlashMessage(null);
              this.router.navigateByUrl('/eintrag-schreiben/' + this.idEString + '/' + idUser + '/' + this.ides).then();
            } else {
              this.setFlashMessage('ueberschrift==null');
            }

          });

    }
  }

}
