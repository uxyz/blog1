import {User} from './user';
import {Ueberschrift} from './ueberschrift';


export interface Eintrag {

  id: number;
  text: string;
  postedAt: Date;
  ldt: Date;
  realTime: string;
  user: User;
  user_id: number;
  userid: number;
  username: string;
  ueberschrift: Ueberschrift;
  ueberschrift_id: number;
  likesnumber: number;
  likesuserid: Set<User>;

}
