import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// import {SecurityService} from '../security.service';
import {Ueberschrift} from '../ueberschrift';
import {Eintrag} from '../eintrag';
import {User} from '../user';


@Component({
  selector: 'app-left-frame',
  templateUrl: './left-frame.component.html',
  styleUrls: ['./left-frame.component.css']
})
export class LeftFrameComponent implements OnInit {

  sessionUser: User | null = null;
  ueberschriftList: Ueberschrift[];
  eintragList: Eintrag[] = [];
  private zahl: string;
  ueberschrift: Ueberschrift;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {

  /*this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      });*/


  this.http.get<Ueberschrift[]>('/api/ueberschrifts')
      .subscribe(ueberschriftList => {
        this.ueberschriftList = ueberschriftList.slice(0, 15);

        for (const uebr of this.ueberschriftList) {
          this.zahl = uebr.id.toString();
          console.log(uebr.eintragList);
          console.log(this.ueberschriftList);
          this.http.get<Eintrag[]>('/api/eintrag/' + this.zahl)

            .subscribe(
              eintragList => {
                this.eintragList = eintragList;
                for (const eintr of this.eintragList) {
                  if (eintr.ueberschrift.id === uebr.id) {
                    uebr.eintragList.push(eintr);
                  }
                }


              });

        }
     //   this.ueberschriftList.sort((a, b) => b.eintragList.length - a.eintragList.length);


      });


  }

}



