import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {UeberschriftService} from '../ueberschrift.service';
import {Ueberschrift} from '../ueberschrift';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import {User} from '../user';
import {SecurityService} from '../security.service';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  constructor(private http: HttpClient,
              private ueberschriftService: UeberschriftService,
              private router: Router, private securityService: SecurityService) {
  }
  sessionUser: User | null = null;

  ueberschrift: Ueberschrift;
  model: any;
  ueberschriftOptions: Ueberschrift [] = [];
  ueberschriftOptionsValues: string [] = [];
  xxx: string;

  @ViewChild('instance', {static: true}) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  ngOnInit(): void {




    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      }
    );

    this.http.get<Ueberschrift[]>('/api/ueberschrifts')
      .subscribe(ueberschriftt => {
        this.ueberschriftOptions = ueberschriftt;
        for (const ueberschrift of this.ueberschriftOptions) {
          this.ueberschriftOptionsValues.push(ueberschrift.text);
        }
      });
  }

  searchKeyword = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;


    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.ueberschriftOptionsValues
        : this.ueberschriftOptionsValues.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }
  search() {
    for (const ueberschrift of this.ueberschriftOptions) {
      if (ueberschrift.text === this.model ) {
        this.http.get<Ueberschrift>('/api/ueberschrift/' + ueberschrift.id.toString())
          .subscribe(ueberschriftttttt => {
              this.ueberschrift = ueberschrift;
              this.ueberschriftService.setUeberschrift(this.ueberschrift);
              this.router.navigate(['/ueberschriftDetail/' + ueberschrift.id.toString()]);
            }

          );
      }else {
        this.router.navigate(['/ueberschrift',  this.sessionUser.id, this.ueberschriftOptions.length + 1]);
      }
    }
  }

}

