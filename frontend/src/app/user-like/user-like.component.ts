import { Component, OnInit } from '@angular/core';
import {UserLike} from '../userLike';
import {User} from '../user';
import {Eintrag} from '../eintrag';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {ActivatedRoute} from '@angular/router';
import {Ueberschrift} from '../ueberschrift';

@Component({
  selector: 'app-user-like',
  templateUrl: './user-like.component.html',
  styleUrls: ['./user-like.component.css']
})
export class UserLikeComponent implements OnInit {

  likesuser: UserLike[] = [];
  private sessionUser: User | null;
  private sessionUser1: User | null;
  eintrags: Eintrag[];
  eintrag: Eintrag;
  private sub: any;
  private id: number;
  private idS: string;
   username: string;
ueberschriftpr: Ueberschrift [] = [];
  constructor(private http: HttpClient, private securityService: SecurityService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;
      });
    this.idS = this.id.toString();
    // const idUser = this.sessionUser.id.toString();
    this.http.get<UserLike[]>('/api/userlikes/' + this.idS)
      .subscribe(likeeintrags => this.likesuser = likeeintrags);

    this.http.get<User>('/api/users/' + this.idS)
      .subscribe(u => {
        this.sessionUser1 = u;
        console.log(this.sessionUser1.username);
        console.log(this.username);
        const idUser = this.sessionUser1.id.toString();
        this.http.get<Ueberschrift[]>('api/ueberschriftsP/' + idUser )
          .subscribe(ueberschriftpr => this.ueberschriftpr = ueberschriftpr);

        this.http.get<Eintrag[]>('/api/eintragS/' + idUser)
          .subscribe(eintraege => this.eintrags = eintraege);

      });



    for (const usrlk of this.likesuser){
      console.log(usrlk);
    }


  }

}
