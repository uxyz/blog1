import {Eintrag} from './eintrag';
import {User} from './user';


export interface Ueberschrift {
  id: number;
  text: string;
  postedAt: Date;
  ldt: Date;
  realTime: string;
  user: User;
  userid: number;
  eintragList: Eintrag[];
  username: string;
 }
