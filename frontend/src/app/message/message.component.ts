import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EintragDTO} from '../eintragDTO';
import {MessageDTO} from '../messageDTO';
import {Eintrag} from '../eintrag';
import {Message} from '../message';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {


  sessionUser: User | null = null;

  postedAt: Date = new Date();
  flashMessage = {message: ''};
  messageData: MessageDTO = {
    text: '',
  };


  private id: number;
  private sub: any;
  private idString: string;
  private idStringY: string;
  private idStringE: string;
  private uid: number;
  username: string;
  message: Message;
  userNameA: string;


  constructor(private http: HttpClient, private securityService: SecurityService, private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;

      this.idString = this.id.toString();
    });

    this.sub = this.route.params.subscribe(params => {
      this.uid = +params.uid;

      this.idStringY = this.uid.toString();
    });

  }





  messageschreiben() {
    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      });

    this.username = this.sessionUser.username;
   // this.idString = this.sessionUser.id.toString();

    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;

      this.idString = this.id.toString();
    });

    this.sub = this.route.params.subscribe(params => {
      this.uid = +params.uid;

      this.idStringY = this.uid.toString();
    });
    console.log(this.uid);

    if (this.messageData.text.length < 1) {
      this.setFlashMessage('you must enter something');
    } else {
      this.http.post<Message>('/api/message/' + this.idString + '/' + this.idStringY, {
        text: this.messageData.text,
        postedAt: this.postedAt,
        user: this.sessionUser,
        username: this.username,
        userid: this.sessionUser.id,
        deletedin: false,
        deletedout: false,
      })
        .subscribe(message => {

         this.message = message;
         if (message != null) {
            this.setFlashMessage(null);
            this.router.navigateByUrl('').then();

          } else {
            this.setFlashMessage('eintrag==null');
          }});
      console.log(this.username);
     // console.log(this.message.userNameA);

    }}

  setFlashMessage(message: string) {
    this.flashMessage.message = message;
  }

}
