import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Message} from '../message';
import {MessageDTO} from '../messageDTO';
import {Eintrag} from '../eintrag';
import {ReactiveFormsModule, FormGroup, FormBuilder, Form} from '@angular/forms';


@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.css']
})
export class MessageBoxComponent implements OnInit {

  sessionUser: User|null = null;
  postedAt: Date = new Date();
  flashMessage = {message: ''};
  messageData: MessageDTO = {
    text: '',
  };


  private id: number;
  private sub: any;
  private idString: string;
  private idStringY: string;
  private idStringE: string;
  private uid: number;
  username: string;
  messages: Message[] = [];
  messageList: Message[];
  message: Message;

  isChecked: boolean;
  iChecked: boolean;
  checkarray: any = [];





  constructor(private http: HttpClient, private securityService: SecurityService, private router: Router,
              private route: ActivatedRoute) {
    this.isChecked = false;

  }


  onclick(message){
    const messageid = message.id.toString();

    this.http.post<Message>('/api/messagelarge/' + messageid, null)
      .subscribe(mess => this.message = mess);


  }


  checked(){
    if (this.isChecked === true)
    {
      this.isChecked = false;
    }
    else
    {
      this.isChecked = true;
    }

  }

  loeschen(message) {
    const  element =  (document.getElementsByName('umut') as any  as HTMLInputElement[]);

    for (const elm of element) {


      if (elm.checked){

        const messageid = elm.value.toString();
        this.http.post<Message>('/api/checkboxin/' + messageid, null)
          .subscribe(mess =>
            message = mess


          );
        this.router.navigateByUrl('');
      }
      console.log(elm.value);
      console.log(elm.checked);
    }
  }


              ngOnInit(): void {


    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      });

    // this.username = this.sessionUser.username;
    // this.idString = this.sessionUser.id.toString();

    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;

      this.idString = this.id.toString();
    });

    /*this.sub = this.route.params.subscribe(params => {
      this.uid = +params.uid;

      this.idStringY = this.uid.toString();
    });
    console.log(this.uid);*/

    this.http.get<Message[]>('/api/message/' + this.idString)

        .subscribe(messages => {

          this.messages = messages;

          for (const message of this.messages){
            console.log(message.userName);
            console.log(message.userid); // gönderenin adi
            console.log(message.deletedin);
            console.log(message);
          }
          console.log(this.messages);

        });


}

/*deletemessage(message) {
  const userid = this.sessionUser.id.toString();
  const messageid = message.id.toString();


  this.http.post<Message[]>('/api/messagedelete/'  + userid + '/' + messageid, null)
    .subscribe(mess => this.messageList = mess );


}*/

  selectall(){
    const element = HTMLInputElement = document.getElementById('checkbox1');
    const isChecked = (element as any as HTMLInputElement).checked;
  }

  deletemessage(message){

    const messageid  = message.id.toString();
    message.deletedin = true;
    console.log(message.deletedin.toString());
    this.http.post<Message>('/api/messagedelete/'   + messageid, null)
      .subscribe(mess =>
        message = mess
        // this.router.navigateByUrl('');

      );
    console.log(message.id);
    console.log(message.deletedin);
    console.log(message);

    // message.deletedin === true;

    /*this.messages.splice(message);
    console.log(this.messages);*/
/*    const userid = this.sessionUser.id.toString();
    const messageid = message.id.toString();


    this.http.post<Message[]>('/api/messagedelete/'  + userid + '/' + messageid, null)
      .subscribe(messlist => this.messages = messlist);*/

  }




}
