package com.example.demo.UserLike;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface UserLikeRepository extends CrudRepository<UserLike, Long> {



    Set<UserLike> findAll();

    Set<UserLike> findAllByUserId(long id);
    Set<UserLike> findByEintragId(long eintragid);

    UserLike findByUserIdAndEintragId(long userId, long eintragId);
}
