package com.example.demo.ueberschrift;


import com.example.demo.eintrag.Eintrag;
import com.example.demo.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class Ueberschrift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;


    @OneToMany
    @JoinColumn(name = "eintrag_id")
    private List<Eintrag> eintragList;

    @Size(max = 500)
    private String text;

    private Instant postedAt;
    private LocalDateTime ldt;
    private String username;
    private long userid;
    private int eintragListLenght;
    private String realTime;



    public Ueberschrift() {
    }




    public Ueberschrift(User user, String text, Instant postedAt, List<Eintrag> eintragList, String username) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
        this.eintragList = eintragList;
        this.ldt = LocalDateTime.ofInstant(postedAt, ZoneId.systemDefault());

        DateFormat newdate = new SimpleDateFormat("dd-MM-yyyy~HH:mm:ss");
        Date realtime1 = new Date();
        this.realTime =newdate.format(realtime1);
        this.username = username;
        this.userid = user.getId();
        this.eintragListLenght = 0;


    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public List<Eintrag> getEintragList() {
        return eintragList;
    }

    public void setEintragList(List<Eintrag> eintragList) {
        this.eintragList = eintragList;
    }

    public LocalDateTime getLdt() {
        return ldt;
    }

    public void setLdt(LocalDateTime ldt) {
        this.ldt = ldt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public int getEintragListLenght() {
        return eintragListLenght;
    }

    public void setEintragListLenght(int eintragListLenght) {
        this.eintragListLenght = eintragListLenght;
    }
    public String getRealTime() {
        return realTime;
    }

    public void setRealTime(String realTime) {
        this.realTime = realTime;
    }
}
