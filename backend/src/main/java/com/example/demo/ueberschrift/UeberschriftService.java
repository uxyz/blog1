package com.example.demo.ueberschrift;


import com.example.demo.eintrag.Eintrag;
import com.example.demo.eintrag.EintragRepository;
import com.example.demo.user.User;
import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Set;

@Service
public class UeberschriftService {

    private UeberschriftRepository ueberschriftRepository;
    private UserService userService;
    private EintragRepository eintragRepository;

    @Autowired
    public UeberschriftService(UeberschriftRepository ueberschriftRepository, UserService userService, EintragRepository eintragRepository){
        this.ueberschriftRepository = ueberschriftRepository;
        this.userService = userService;
        this.eintragRepository =eintragRepository;
    }
    public Set<Ueberschrift> getAllUeberschrifts(){
        return ueberschriftRepository.findAll();
    }




    public Ueberschrift ueberschriftSchreiben(String text, long id, Instant postedAt, long user_id, String username){
        User user = userService.getUserById(user_id);
       List<Eintrag> eintragList = eintragRepository.findEintragByUeberschriftId(id);
        return ueberschriftRepository.save(new Ueberschrift(user,text,postedAt,eintragList,username));
    }
    public Ueberschrift getUeberschriftById(long id){
        return ueberschriftRepository.findById(id);
    }

}
