package com.example.demo.ueberschrift;


import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class UeberschriftController {

    private UeberschriftService ueberschriftService;
    private UserService userService;
    private UeberschriftRepository ueberschriftRepository;

    @Autowired
    public UeberschriftController (UeberschriftService ueberschriftService, UserService userService,
                                   UeberschriftRepository ueberschriftRepository){
        this.ueberschriftService=ueberschriftService;
        this.userService=userService;
        this.ueberschriftRepository = ueberschriftRepository;
    }


    @GetMapping("/api/ueberschrift/{ueberschriftId}")
    public Ueberschrift showUberschrift(@PathVariable("ueberschriftId") long id){
        return ueberschriftService.getUeberschriftById(id);
    }

    @GetMapping("/api/ueberschrifts")
    public Set<Ueberschrift> showAllUeberschrifts(){
        return ueberschriftService.getAllUeberschrifts();
    }


    @GetMapping("/api/ueberschrift")
    public Set<Ueberschrift> showAllUeberschrift(){
        return ueberschriftService.getAllUeberschrifts();
    }



    @PostMapping("/api/ueberschrift/{userId}")
    public Ueberschrift ueberschriftSchreiben(@RequestBody Ueberschrift ueberschrift, @PathVariable("userId") long user_id){
        return ueberschriftService.ueberschriftSchreiben(ueberschrift.getText(),ueberschrift.getId(),ueberschrift.getPostedAt(),user_id,ueberschrift.getUsername());
    }
    @GetMapping("/api/ueberschriftsP/{userId}")
    public Set<Ueberschrift> showUeberschrifs(@PathVariable("userId") long user_id){

        return ueberschriftRepository.findByUserId(user_id);

    }
}
