package com.example.demo;


import com.example.demo.eintrag.EintragService;
import com.example.demo.message.MessageController;
import com.example.demo.message.MessageService;
import com.example.demo.ueberschrift.UeberschriftService;
import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataService {

    private UeberschriftService ueberschriftService;
    private UserService userService;
    private EintragService eintragService;
    private MessageService messageService;
    private MessageController messageController;

    @Autowired
    public DataService(UeberschriftService ueberschriftService,UserService userService,EintragService eintragService,MessageService messageService,MessageController messageController) {
        this.ueberschriftService=ueberschriftService;
        this.userService=userService;
        this.eintragService=eintragService;
        this.messageService =messageService;
        this.messageController = messageController;
        getData();
    }

    public void getData(){
        if (!userService.usernameExists("test1")) {
            userService.register("test1", "test1");
            userService.register("umut","12345");

        }

    }
}
