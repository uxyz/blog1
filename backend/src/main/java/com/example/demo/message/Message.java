package com.example.demo.message;


import com.example.demo.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;


    @ManyToMany
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private List<User> user;

    @Size(max = 4000)
    private String text;

    private Instant postedAt;
    private LocalDateTime ldt;
    private String userName;
    private String realTime;
    private String userNameA;
    private long userid;     // mesaji gönderenin!!!
    private long user_id; // mesaji alanin idsi
    private boolean deletedin;
    private boolean deletedout;
    private boolean checkboxin;
    private boolean checkboxout;
    private boolean readmessage;



    public Message() {

    }


    public Message( List<User> user, String text, Instant postedAt, String userName,String userNameA,long userid, long user_id) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
        this.ldt = LocalDateTime.ofInstant(postedAt, ZoneId.systemDefault());
        DateFormat newdate = new SimpleDateFormat("dd-MM-yyyy~HH:mm:ss");
        Date realtime1 = new Date();
        this.realTime =newdate.format(realtime1);
        this.userName = userName;
        this.userNameA = userNameA;
        this.userid= userid;
        this.user_id = user_id;
        this.deletedin = false;
        this.deletedout = false;
        this.checkboxin = false;
        this.checkboxout = false;
        this.readmessage = false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public LocalDateTime getLdt() {
        return ldt;
    }

    public void setLdt(LocalDateTime ldt) {
        this.ldt = ldt;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public String getRealTime() {
        return realTime;
    }

    public void setRealTime(String realTime) {
        this.realTime = realTime;
    }

    public String getUserNameA() {
        return userNameA;
    }

    public void setUserNameA(String userNameA) {
        this.userNameA = userNameA;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public boolean isDeletedin() {
        return deletedin;
    }

    public void setDeletedin(boolean deletedin) {
        this.deletedin = deletedin;
    }

    public boolean isDeletedout() {
        return deletedout;
    }

    public void setDeletedout(boolean deletedout) {
        this.deletedout = deletedout;
    }

    public boolean isCheckboxin() {
        return checkboxin;
    }

    public void setCheckboxin(boolean checkboxin) {
        this.checkboxin = checkboxin;
    }

    public boolean isCheckboxout() {
        return checkboxout;
    }

    public void setCheckboxout(boolean checkboxout) {
        this.checkboxout = checkboxout;
    }

    public boolean isReadmessage() {
        return readmessage;
    }

    public void setReadmessage(boolean reads) {
        this.readmessage = reads;
    }
}
