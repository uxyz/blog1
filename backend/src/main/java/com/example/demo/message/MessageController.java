package com.example.demo.message;


import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MessageController {


    private MessageService messageService;
    private UserService userService;
    private MessageRepository messageRepository;

    @Autowired
    public MessageController(MessageService messageService, UserService userService, MessageRepository messageRepository
    ) {
        this.messageService = messageService;
        this.userService = userService;
        this.messageRepository = messageRepository;


    }

    @PostMapping("/api/message/{sessionUserId}/{userid}")
    public Message messageschreiben(@RequestBody Message message, @PathVariable("sessionUserId") long sessionUserId,
                                    @PathVariable("userid") long userid) {

        //messageService.messageSchreiben(message.getUser())


        return messageService.messageSchreiben(message.getText(), message.getPostedAt(), sessionUserId, message.getUserName(), userid);

    }

    @GetMapping("/api/message/{userId}")

    public List<Message> getmessages(@PathVariable("userId") long userId) {

        return messageRepository.findByUserIdOrderByPostedAtDesc(userId);


    }


    @PostMapping("/api/messagedelete/{messageid}")
    public Message deleteMessage(@PathVariable("messageid") long messageid) {
        System.out.println(this.messageRepository.findMessageById(messageid).isDeletedin());
        this.messageRepository.findMessageById(messageid).setDeletedin(true);

        // this.ueberschriftService.getUeberschriftById(uebershriftid).setEintragListLenght(this.ueberschriftService.getUeberschriftById(uebershriftid).getEintragListLenght()-1);

        //  return this.messageService.deletemessage(messageid);
        System.out.println(this.messageService.getMessageRepository().findMessageById(messageid).isDeletedin());
        Message message = this.messageRepository.findMessageById(messageid);
        return this.messageRepository.save(message);

    }

    @PostMapping("/api/messagedeleteo/{messageid}")
    public Message deleteMessageo(@PathVariable("messageid") long messageid) {
        //   System.out.println(this.messageRepository.findMessageById(messageid).isDeletedin());
        this.messageRepository.findMessageById(messageid).setDeletedout(true);

        // this.ueberschriftService.getUeberschriftById(uebershriftid).setEintragListLenght(this.ueberschriftService.getUeberschriftById(uebershriftid).getEintragListLenght()-1);

        //  return this.messageService.deletemessage(messageid);
        // System.out.println(this.messageService.getMessageRepository().findMessageById(messageid).isDeletedin());
        Message message = this.messageRepository.findMessageById(messageid);
        return this.messageRepository.save(message);

    }


    @PostMapping("/api/checkboxout/{messageid}")
    public Message checkboxout(@PathVariable("messageid") long messageid) {
        //   System.out.println(this.messageRepository.findMessageById(messageid).isDeletedin());
        this.messageRepository.findMessageById(messageid).setCheckboxout(true);

        // this.ueberschriftService.getUeberschriftById(uebershriftid).setEintragListLenght(this.ueberschriftService.getUeberschriftById(uebershriftid).getEintragListLenght()-1);

        //  return this.messageService.deletemessage(messageid);
        // System.out.println(this.messageService.getMessageRepository().findMessageById(messageid).isDeletedin());
        Message message = this.messageRepository.findMessageById(messageid);
        return this.messageRepository.save(message);

    }

    @PostMapping("/api/checkboxin/{messageid}")
    public Message checkboxin(@PathVariable("messageid") long messageid) {
        //   System.out.println(this.messageRepository.findMessageById(messageid).isDeletedin());
        this.messageRepository.findMessageById(messageid).setCheckboxin(true);

        // this.ueberschriftService.getUeberschriftById(uebershriftid).setEintragListLenght(this.ueberschriftService.getUeberschriftById(uebershriftid).getEintragListLenght()-1);

        //  return this.messageService.deletemessage(messageid);
        // System.out.println(this.messageService.getMessageRepository().findMessageById(messageid).isDeletedin());
        Message message = this.messageRepository.findMessageById(messageid);
        return this.messageRepository.save(message);

    }

    @PostMapping("/api/messagelarge/{messageid}")
    public Message messageread(@PathVariable("messageid") long messageid) {
        this.messageRepository.findMessageById(messageid).setReadmessage(true);
        Message message = this.messageRepository.findMessageById(messageid);
        return this.messageRepository.save(message);
    }

    @GetMapping("/api/messageread/{messageid}")
    public Message messagereaded(@PathVariable("messageid") long messageid) {


        return this.messageRepository.findMessageById(messageid);

    }
}
