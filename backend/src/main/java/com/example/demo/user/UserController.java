package com.example.demo.user;



import com.example.demo.security.RegistrationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * The PostMapping method is used for registering a user, according to the RegistrationDTO object
     *
     * @param registrationDTO is a RegistrationDTO object, passed as a RequestBody
     * @return the return value is a User
     */
    @PostMapping("/api/register")
    public User register(@RequestBody RegistrationDTO registrationDTO) {
        return userService.register(registrationDTO.getUsername(), registrationDTO.getPassword1());
    }

    @GetMapping("/api/user/{userId}")
    public User findUser(@PathVariable ("userId") long id){
        return userService.getUserById(id);
    }

    @GetMapping("/api/users/{userId}")
    public User findUsers(@PathVariable ("userId") long id){
        return userService.getUserById(id);
    }


}
