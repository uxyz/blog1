package com.example.demo.eintrag;


import com.example.demo.UserLike.UserLike;
import com.example.demo.ueberschrift.Ueberschrift;
import com.example.demo.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import java.util.Date;
import java.util.Set;

@Entity
public class Eintrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @ManyToOne
    @JoinColumn(name = "ueberschrift_id")
    private Ueberschrift ueberschrift;

    @Size(max = 4000)
    private String text;
    private Instant postedAt;
    private LocalDateTime ldt;
    private String username;
    private long likesnumber;
    private long userid;
    private String realTime;





    public Eintrag() {
    }



    public Eintrag(User user, Ueberschrift ueberschrift, String text, Instant postedAt, String username) {
        this.user = user;
        this.ueberschrift = ueberschrift;
        this.text = text;
        this.postedAt = postedAt;
        this.username = username;
        this.likesnumber = 0;
        this.ldt = LocalDateTime.ofInstant(postedAt, ZoneId.systemDefault());
        DateFormat newdate = new SimpleDateFormat("dd-MM-yyyy~HH:mm:ss");
        Date realtime1 = new Date();
        this.realTime =newdate.format(realtime1);
        this.userid = user.getId();

    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Ueberschrift getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(Ueberschrift ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public long getLikesnumber() {
        return likesnumber;
    }

    public void setLikesnumber(long likesnumber) {
        this.likesnumber = likesnumber;
    }

    public LocalDateTime getLdt() {
        return ldt;
    }

    public void setLdt(LocalDateTime ldt) {
        this.ldt = ldt;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getRealTime() {
        return realTime;
    }

    public void setRealTime(String realTime) {
        this.realTime = realTime;
    }
}
